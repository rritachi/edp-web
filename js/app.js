window.App = Ember.Application.create();

App.ApplicationAdapter = DS.RESTAdapter.extend({
    namespace: 'v0.1'
});

App.Store = DS.Store.extend({
	adapter: 'App.ApplicationAdapter',
	revision: 12,
	url: 'http://code-001.local/v0.1/'
});