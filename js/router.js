App.Router.map(function() {
  	this.resource('profile', { path: '/profile' });
  	this.resource('login', { path: '/' });
});

App.LoginRoute = Ember.Route.extend({
	setupController: function(controller, model) {
    	controller.set('profile', model);
  	}
});

App.ProfileRoute = Ember.Route.extend({
	setupController: function(controller, model) {
    	controller.set('profile', model);
  	},
  	model: function(params) {
  		return this.store.find('/users', params.token);
  	}
});