

App.LoginController = Ember.Controller.extend({
  	login: function() {
  		var store = this.store;
  		var self = this;
	    Ember.$.post("http://code-001.local/v0.1/auth/login",{
	    	username: this.get("username"),
		    password: this.get("password"),
		    dataType: 'JSON'
	    }).then(function(data){

	    	if (data.status == 1) {
	    		var profile = store.push('profile', {
	    			id: 1,
		    		token: data.results.token,
		    		username: data.results.username
		    	});

		    	profile.save();

		    	self.transitionTo('profile');
	    	}
	    	
	    });
	}
});