var attr = DS.attr;

App.Profile = DS.Model.extend({
	token: attr('string'),
	username: attr('string')
});